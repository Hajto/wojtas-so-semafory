#include <iostream>
#include "system_call.h"
#include "fight_spawner.h"
#include "semaphore_control.h"

int main(int argc, char *argv[]) {
    int semid = Semaphore::init_semaphore(1);
    auto spawner = FightSpawner::build_spawner_from_args(argc, argv);


//    int count = spawner();
//    build_spawner_from_args(argc, argv);
//
//    sleep(2);
//    semaphore_unlock(semid,0);
//    std::cout<<"Semaphor blocking procecesses from race has been opened. Time to start the race to the resource!"
//             <<std::endl;
//
//    for(int i = 0; i < count; i++){
//        int code = 0;
//        int result = wait(&code);
//
//        printf("Proces %d with pid %d exited with status code: %d\n", i, result, code);
//    }
//
//    sleep(2);
//    clear_sempahores(semid);
    return 0;
}