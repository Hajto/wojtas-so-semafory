#include <iostream>
#include "unistd.h"
#include "semaphore_control.h"

void critical_section(int semid, unsigned short semaphor_number);

int main(int argc, char *argv[]){
    int semid = Semaphore::get_shared_semaphore(1);

    std::cout<<"Calling from child arg count is "<<argc<<std::endl;
    std::cout<<"Semid: "<<semid<<std::endl;
    for(int i = 0; i < argc; i++){
        std::cout<<"Argument "<<i <<":"<<argv[i]<<std::endl;
    }

    int how_many_executions = std::stoi(argv[1]);
    for(int i = 0; i < how_many_executions; i++){
        critical_section(semid, 0);
    }
}

void critical_section(int semid, unsigned short semaphor_number){
    Semaphore::semaphore_await(semid, semaphor_number);
    std::cout<<"I am process:"<<getpid()<<" and I am in critical section!"<<std::endl;
//    sleep(1);
    Semaphore::semaphore_unlock(semid, semaphor_number);
}