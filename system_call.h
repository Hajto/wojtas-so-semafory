//
// Created by Haito on 13.12.2017.
//

#ifndef WOJTAS_SEMAFORY_SYSTEM_CALL_H
#define WOJTAS_SEMAFORY_SYSTEM_CALL_H

#include <string>

namespace SystemCall {
    std::string call_command(const std::string &command);
}

#endif //WOJTAS_SEMAFORY_SYSTEM_CALL_H
