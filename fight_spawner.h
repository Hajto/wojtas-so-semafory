//
// Created by Haito on 13.12.2017.
//

#ifndef WOJTAS_SEMAFORY_FIGHTSPAWNER_H
#define WOJTAS_SEMAFORY_FIGHTSPAWNER_H

#include <string>
#include <functional>

namespace FightSpawner {
    std::function<int (void)> build_spawner_from_args(int argc, char **argv);
    void spawnChild(const std::string &path, const std::string &program, int execution_count);
    void spawnChildren(const std::string &program, int program_count, int execution_count);
}

#endif //WOJTAS_SEMAFORY_FIGHTSPAWNER_H
