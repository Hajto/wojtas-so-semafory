#include "fight_spawner.h"
#include <iostream>
#include "semaphore_control.h"
#include "system_call.h"
#include <unistd.h>

// Private functions for namespace start
int get_available_process_count();
void load_program(const std::string &path, const std::string &program, int execution_count);
// Private functions for namespace end

std::function<int (void)> FightSpawner::build_spawner_from_args(int argc, char **argv) {
    if(argc != 4){
        std::cout<<"Wrong number of arguments got:"
                 <<argc
                 <<" while expecting to get 3"
                 <<std::endl
                 <<"Listing arguments:"
                 <<std::endl;

        for(int i = 0; i < argc; i++){
            std::cout<<"Arg:"<<i<<":"<<argv[i]<<std::endl;
        }
        exit(1);
    } else {
        std::cout<<argv[1]<<std::endl;
        std::string program_name = std::string(argv[1]);
        int program_count = std::stoi(argv[2]);
        long long execution_count = std::stol(argv[3]);
        int available_procs = get_available_process_count();

        if(program_count <= 0 || execution_count <= 0 || program_count >= available_procs){
            std::cout<<"Both numerical arguments must > 0 and lower than "<<available_procs<<std::endl;
            exit(1);
        }

        return [=](){ //Capture mode musi być =, aby skopiowało
            spawnChildren(program_name, program_count, execution_count);
            return program_count;
        };
    }
}

void FightSpawner::spawnChild(const std::string &path, const std::string &program, int execution_count) {
    switch(fork()){
        case 0:
            load_program(path, program, execution_count);
            break;
        case -1:
            printf("Failed to execl");
            exit(1);
        default:
            break;
    }
}

void load_program(const std::string &path, const std::string &program, int execution_count) {
    int spawn_result = execl((path+program).c_str(), program.c_str(), std::to_string(execution_count).c_str(), NULL);
    if(spawn_result != 0){
        std::cout<< "Exec failed, process: "
                 <<getpid()
                 <<" won't do any work"
                 <<std::endl;
    }
}

int get_available_process_count() {
    std::string max_processes = SystemCall::call_command("ulimit -u");
    std::string current_procs = SystemCall::call_command("ps -ax | wc -l");

    return std::atoi(max_processes.c_str()) - std::atoi(current_procs.c_str());
}

void FightSpawner::spawnChildren(const std::string &program, int program_count, int execution_count) {
    std::string path = Semaphore::get_path() + "/";
    std::cout<<"Spawning program: "
             <<path
             <<program
             <<std::endl;
    for(int i = 0; i < program_count; i++){
        spawnChild(path, program, execution_count);
    }
}
